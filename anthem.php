

<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation - Anthem</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.
">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <meta name="author" content="">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->


<?php include 'include/header-inner.php' ?>


  <!-- <section class="graduation-miles-banner registraion-banner">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-6">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                    Habib University  
                    <span>Anthem</span>
                 </h1>
              </div>
           </div>
           <div class="col-lg-6">
              <div class="set-banner-rght-img">
                 <img src="img/anthem/banner.png" class="img-fluid" alt="">
                 <a  data-fancybox="gallery" class="play-icon-box" href="https://youtu.be/iOcjErcxYQs" data-fancybox="gallery">
                     <img src="img/playicon.svg" alt="">
                  </a>
              </div>
           </div>
        </div>
     </div>
  </section> -->
  <section class="graduation-miles-banner banner-26">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-6">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                 Habib University  
                    <span>Anthem</span>
                 </h1>
                 <div class="set-banner-rght-img">
                 <!-- <img src="img/anthem/banner.png" class="img-fluid" alt=""> -->
                 <a  data-fancybox="gallery" class="play-icon-box" href="https://youtu.be/iOcjErcxYQs" data-fancybox="gallery">
                     <img src="img/playicon.svg" alt="">
                  </a>
              </div>
              </div>
           </div>
        </div>
     </div>
  </section>

  <section class="para-area">
   <div class="container">
      <section class="sec-heading">
         <!-- <h5>Be the next generaton of</h5> -->
         <h1>#HUAnthem</h1>
         <p>The Habib University Anthem encapsulates the spirit of what it means to be a Habib University student.<br> It captures in its lines the very essence of a being Habib student, someone who is a seeker of knowledge,<br> who is filled with the promise of a better tomorrow and is willing to play their part in it.</p>
      </section>
   </div>
 </section>

<section class="registaion-form-area">
   <div class="container">
      <div class="row justify-content-between">
         <div class="col-lg-6 col-sm-6">
            <div class="rgist-box anthem-box">
               <a href="img/anthem/anthem-eng-lg.png" data-fancybox="gallery">
                  <img src="img/anthem/anthem-eng.png" class="img-fluid" alt="Habib University Anthem">
               </a>
            </div>
         </div>
         <div class="col-lg-6 col-sm-6">
            <div class="rgist-box anthem-box">
               <a href="img/anthem/anthem-urdu-lg.png" data-fancybox="gallery">
                  <img src="img/anthem/anthem-urdu.png" class="img-fluid" alt="Habib University Anthem">
               </a>
            </div>
         </div>
         
      </div>
   </div>
</section>




<?php include 'include/footer.php' ?>