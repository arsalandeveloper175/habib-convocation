
//   Home Page JS

$(function () {

    $('.cermony-img').owlCarousel({
        loop:true,
        nav: false,
        navText: ['<img src="img/left-arrow.svg">', '<img src="img/right-arrow.svg">'],
        dots: true,
        autoplay:true,
        autoplayHoverPause:false,
        mouseDrag: false,
        responsiveClass:true,
        autoplayTimeout:5000,
        responsive:{
            0:{
                items:1,
                autoplay:true,
                nav: false,
                dots: true,
            },                   
            576:{
                items:1,
                loop:true
            },
            768: {
                items: 1,
                loop:true,
            }, 
            992: {
                items: 1,
                loop:true
            }
        }
    });



    if($(window).width() <= 768){
        if(('.coomon-responsove-slider').length != 0){
            $('.coomon-responsove-slider').addClass('owl-carousel owl-theme');
            $('.coomon-responsove-slider').owlCarousel({
                loop:false,
                margin:20,
                dots: true,
                nav:false,
                navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
                autoplay:false,
                autoplayTimeout:2000,
                autoplayHoverPause:false,
                mouseDrag: true,
                responsive:{
                    0:{
    
                        items:1
                    }, 
                    480:{
    
                        items:1
                    },
                    574:{
    
                        items:1
                    },
                    766:{
                        items:1
                    }
                }
            });
        }
    }
    if($(window).width() <= 768){
        if(('.regalia-responsove-slider').length != 0){
            $('.regalia-responsove-slider').addClass('owl-carousel owl-theme');
            $('.regalia-responsove-slider').owlCarousel({
                loop:false,
                margin:20,
                dots: true,
                nav:false,
                navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
                autoplay:false,
                autoplayTimeout:2000,
                autoplayHoverPause:false,
                mouseDrag: true,
                responsive:{
                    0:{
    
                        items:1
                    }, 
                    480:{
    
                        items:1
                    },
                    574:{
    
                        items:2
                    },
                    766:{
                        items:2
                    }
                }
            });
        }
    }
    

    if($(window).width() <= 768){
        if(('.awards-responsove-slider').length != 0){
            $('.awards-responsove-slider').addClass('owl-carousel owl-theme');
            $('.awards-responsove-slider').owlCarousel({
                loop:false,
                margin:20,
                dots: true,
                nav:false,
                navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
                autoplay:false,
                autoplayTimeout:2000,
                autoplayHoverPause:false,
                mouseDrag: true,
                responsive:{
                    0:{
    
                        items:1
                    }, 
                    480:{
    
                        items:1
                    },
                    574:{
    
                        items:1
                    },
                    766:{
                        items:1
                    }
                }
            });
        }
    }
    
   
$('.main-slider').owlCarousel({
    loop:true,
    nav: true,
    navText: ['<img src="img/left-arrow.svg">', '<img src="img/right-arrow.svg">'],
    dots: false,
    autoplay:true,
    autoplayHoverPause:false,
    mouseDrag: false,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            autoplay:true,
            nav: false,
            dots: true,
        },                   
        576:{
            items:1,
            loop:true
        },
        768: {
            items: 1,
            loop:true,
        }, 
        992: {
            items: 1,
            loop:true
        }
    }
});


AOS.init({
    disable: function() {
        return window.innerWidth < 800;
    }
});



});





  $(document).ready(function(){
	$('#nav-icon3').click(function(){
		$(this).toggleClass('open');
	});



    // Full Page menu
    $('#toggle').click(function() {
        $(this).toggleClass('active');
        $('#overlay').toggleClass('open');
       });
    // Full Page menu

   
   
      
     
});



// Set the date we're counting down to
var countDownDate = new Date("June 4, 2022 18:00:00").getTime();

// Update the count down every 1 second
var countdownfunction = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now an the count down date
  var distance = countDownDate - now;
 
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  
  if (days < 10 ) {
    days = '0' + days;
  }
  if (hours < 10 ) {
    hours = '0' + hours;
  }
  if (minutes < 10 ) {
    minutes = '0' + minutes;
  }
  if (seconds < 10 ) {
    seconds = '0' + seconds;
  }

  // Output the result in an element with id="custom-timer"
  document.getElementById("custom-timer").innerHTML = 
  "<div class='timer-box'>" +
    "<span class='timer-time'>" + days + "</span>" + "<span class='timer-text'>Days</span> " +
  "</div>" +
  "<div class='timer-box'>" +
     "<span class='timer-time'>" + hours + "</span>" + "<span class='timer-text'>Hours</span> " +
  "</div>" +
  "<div class='timer-box'>" +
      "<span class='timer-time'>" + minutes + "</span>" + "<span class='timer-text'>Minutes</span> " +
  "</div>" +
  "<div class='timer-box'>" +
    "<span class='timer-time'>" + seconds+ "</span>" + "<span class='timer-text'>seconds</span> "
  "</div>";
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(countdownfunction);
    document.getElementById("custom-timer").innerHTML = "EXPIRED";
  }
}, 1000);



$(function () {
    $(".ig_post_container").slice(0, 6).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $(".ig_post_container:hidden").slice(0, 6).slideDown();
        if ($(".ig_post_container:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});




