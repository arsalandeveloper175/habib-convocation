<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/convocation-thumb.jpg" />
	   <meta property="og:image:width" content="1920px" />
       <meta property="og:image:height" content="1080px" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
      <!-- Template CSS -->
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="css/breakpoints.css">
      <link rel="stylesheet" href="css/plugin.css">
      <!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-51636538-33"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-51636538-33');
</script>

   </head>
   <body>
  

      <!-- Main Slider -->
      <section class="main-slider-area">
         <div class="container-fluid-p-0">
            <div class="row no-gutters">
               <!-- Side Nav -->
               <div class="col-xl-3">
                  <div class="side-main-nav navbar-expand-lg side-nav-404">
                     <div class="logo">
                        <img src="img/logo.svg" alt="" class="img-fluid">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                           <div id="nav-icon3">
                               <span></span>
                               <span></span>
                               <span></span>
                               <span></span>
                           </div>
                       </button>
                     </div>
                     <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <?php include 'include/header-home.php' ?>
                     </div>
                  </div>
               </div>
               <!-- Side Nav -->
               <!-- Slider -->
               <div class="col-xl-9">
                  <div class="page-not-found">
                        <h1>404 Not Found</h1>
                        <p>It seems like you have accessed a link that is not available. Please contact <a href="mailto:ebmaster@habib.edu.pk"> webmaster@habib.edu.pk</a> for related queries.</p>
                  
                        <p class="copy-right">© Habib University. All rights reserved.</p>
                  </div>
                  
               </div>
               <!-- Slider -->
            </div>
         </div>
      </section>
      <!-- Main Slider -->
     
  


 

      <script src="js/plugin.js"></script>
      <script src="js/custom.js"></script>
      <!-- <script src="https://apps.elfsight.com/p/platform.js" defer></script> -->
   </body>
</html>
  