<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/convocation-thumb.jpg" />
	   <meta property="og:image:width" content="1920px" />
       <meta property="og:image:height" content="1080px" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
      <!-- Template CSS -->
      <link rel="stylesheet" href="css/style.css?v=2.1.0">
      <link rel="stylesheet" href="css/breakpoints.css?v=2.1.0">
      <link rel="stylesheet" href="css/plugin.css?v=2.1.0">
      <!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-51636538-33"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-51636538-33');
</script>



<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2HCDMP5P7W"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());



gtag('config', 'G-2HCDMP5P7W');
</script>

   </head>
   <body>
  


      <!-- Main Slider -->
      <section class="main-slider-area">
         <div class="container-fluid-p-0">
            <div class="row no-gutters">
               <!-- Side Nav -->
               <div class="col-xl-3 col-lg-4">
                  <div class="side-main-nav navbar-expand-lg">
                     <div class="logo">
                        <img src="img/logo.svg" alt="" class="img-fluid">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                           <div id="nav-icon3">
                               <span></span>
                               <span></span>
                               <span></span>
                               <span></span>
                           </div>
                       </button>
                     </div>
                     <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <?php include 'include/header-home.php' ?>
                     </div>
                  </div>
               </div>
               <!-- Side Nav -->
               <!-- Slider -->
               <div class="col-xl-9 col-lg-8">
                     <a href="#" class="register-btn">
                         <span data-hover="Register">Register</span>
                     </a>
                  <div class="slider-wraperr">
                     <div class="timmer-inner">
                         <ul class="timer-top">
                            <li>
                               <h1>4<sup>th</sup> <span> June 2022</span></h1>
                            </li>
                            <li>
                               <p> 
                                  <strong> Convocation</strong> 
                                   Class of 2022
                              </p>
                            </li>
                         </ul>
                        <div class="flip-timer">
                           <p id="custom-timer"></p>
                        </div>
                     </div>
                     <div class="main-slider owl-carousel owl-theme">
                        <div class="slider-item">
                           <img class="img-fluid slider-img" src="img/slider.jpg" alt="">
                           <img class="img-fluid res-slid" src="img/slider-resp/1.jpg" alt="">
                           <!-- <a class="play-icon-box" href="https://www.youtube.com/watch?v=1R1OHMfwwvw&list=PLVyH-94EYRpqwQBH-JRPbXYIRpkdRpH_Z&ab_channel=HabibUniversity" data-fancybox="gallery">
                              <img src="img/playicon.svg" alt="">
                           </a> -->
                        </div>
                        <div class="slider-item">
                           <a href="javascript:;">
                              <img class="img-fluid slider-img" src="img/slider1.jpg" alt="">
                              <img class="img-fluid res-slid" src="img/slider-resp/2.jpg" alt="">
                           </a>
                        </div>
                        <div class="slider-item">
                           <a href="javascript:;">
                              <img class="img-fluid slider-img" src="img/slider2.jpg" alt="">
                              <img class="img-fluid res-slid" src="img/slider-resp/3.jpg" alt="">
                           </a>
                        </div>
                        <!-- <div class="slider-item">
                           <a href="https://habib.edu.pk/HU-news/hasan-ul-haq-habib-universitys-valedictorian-for-2021/" target="_blank">
                              <img class="img-fluid slider-img" src="img/slider5.jpg" alt="">
                              <img class="img-fluid res-slid" src="img/slider-resp/6.jpg" alt="">
                           </a>
                        </div> -->
                     </div>
                  </div>
               </div>
               <!-- Slider -->
            </div>
         </div>
      </section>
      <!-- Main Slider -->
      <!-- Previouse Comments -->
      <section class="previous-comments">
         <div class="container-fluid p-0">
            <div class="row no-gutters">
               <div class="col-xl-3 col-sm-5 col-md-4">
                  <div class="previous-left">
                     <!-- <h2>Previous Commencements</h2> -->
                     <div class="convo-conte">

                        <h2 class="convo-head-md">convocation
                           <span>2021</span>
                        </h2>
                        <h1 class="convo-head-lg convo-head-lsm">HIGHLIGHTS</h1>
                     </div>
                     <a data-fancybox="gallery" href="https://www.youtube.com/watch?v=tursvx9Ih9I&list=PLVyH-94EYRpqwQBH-JRPbXYIRpkdRpH_Z&index=18&ab_channel=HabibUniversity" class="play-icon-box">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
               <div class="col-xl-9 col-sm-7 col-md-8">
                  <div class="comment-row">
                        <!-- Previcouse Conetent -->
                        <div class="previous-comments-content">
                           <div class="para-mob-scrol">
                              <p>This year, our 5th Convocation, will see the Class of 2022 receive their degrees at the culmination of four years of a unique intellectual experience. As is tradition it will also provide us with the opportunity to celebrate our students’ academic achievements and recognize the commitment and teaching excellence of our faculty.</p>
                              <p class="mt-2">Convocation 2022 is made particularly special as it signals the resumption of the traditional in-person ceremony, the last two years having been affected by the Covid pandemic. We look forward to welcoming all graduands and their families to the campus for this momentous occasion.</p>
                             <p class="mt-2">The event will adhere to Covid-19 health and safety protocols. </p>
                           </div>
                           <h4>Previous Commencements</h4>
                           <ul class="year-abstracts">
                              <li><a href="convocation-2018">2018</a></li>
                              <li><a href="convocation-2019">2019</a></li>
                              <li><a href="convocation-2020">2020</a></li>
                              <li><a href="convocation-2021">2021</a></li>
                           </ul>
                        </div>
                        <!-- Previcouse Conetent -->
                        <!-- Socail Item -->
                        <div class="social-item">
                           <h5>Follow us</h5>
                           <ul>
                              <li><a target="_blank" href="https://www.facebook.com/HabibUniversity"><i class="fab fa-facebook-square"></i></a></li>
                              <li><a target="_blank" href="https://www.linkedin.com/company/habib-university"><i class="fab fa-linkedin"></i></a></li>
                              <li><a target="_blank" href="https://instagram.com/habibuniversity"><img src="img/instagram.png" alt=""></a></li>
                              <li><a target="_blank" href="https://twitter.com/habibuniversity"><i class="fab fa-twitter"></i></a></li>
                              <li><a target="_blank" href="https://www.youtube.com/user/HabibUni"><img src="img/youtube.png" alt=""></a></li>
                              <!-- <li><a href=""> Follow us</a></li> -->
                           </ul>
                        </div>
                        <!-- Socail Item -->
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Previouse Comments -->
      <!-- Division Box -->
      <section class="division-box">
         <div class="container-md container-fluid">
            <div class="row justify-content-between">
               <div class="col-lg-6 col-sm-7">
                  <div class="divsion-content">
                     <h3>HONORING THE CLASS OF 2022</h3>
                     <p class="pad-para">The Class of 2022 is our fifth batch to graduate from <strong> Habib University</strong>. They are graduating at a time when hope for a new beginning has been awakened. The Covid-19 pandemic is receding and the future promises a world of unexplored possibilities. We are confident that the Class of 2022 will become engaged, empathetic and thoughtful leaders of the future, guiding Pakistan and the world as it recovers from the effects of the pandemic.</p>
                  </div>
               </div>
               <div class="col-lg-5 col-sm-5">
                  <div class="division-img">
                     <img src="img/convo-homepage-img-1.png" alt="" class="division-mage" alt="HONORING THE CLASS OF 2022">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->
      <!-- Next Generation -->
      <section class="next-genration">
         <div class="container">
            <section class="sec-heading">
               <h5>Be the next generaton of</h5>
               <h1>LEADERS, THINKERS, CREATORS</h1>
            </section>
            <div class="row justify-content-between coomon-responsove-slider">
               <div class="generation-box-parent">
                  <div class="generation-box">
                     <a href="graduation-milestone">
                        <img src="img/generation-box.png" alt="Graduation Milestones">
                        <div class="gene-content">
                           <h5><strong>  Graduation</strong></h5>
                           <h3> MILESTONES</h3>
                           <p> Make sure you are fully prepared and ready to enjoy your day with our step by step list.</p>
                        </div>

                     </a>
                  </div>
               </div>
               <div class="generation-box-parent">
                  <div class="generation-box">
                     <a href="convocation-speaker">
                        <img src="img/generation-box1.png" alt="About Speakers">
                        <div class="gene-content">
                           <h5><strong>  About</strong></h5>
                           <h3>  Speakers</h3>
                           <p> Words of wisdom and encouragement from <br> community leaders.  </p>
                        </div>
                     </a>
                  </div>
               </div>
               <div class="generation-box-parent">
                  <div class="generation-box">
                     <a href="student-faculty-awards">
                        <img src="img/generation-box2.jpg" alt="Student & Faculty Awards">
                        <div class="gene-content">
                           <h5><strong>  Student & Faculty</strong></h5>
                           <h3>  AWARDS</h3>
                           <p>  Awarded annually to promising students & faculty.</p>
                        </div>
                     </a>
                  </div>
               </div>
               <div class="generation-box-parent">
                  <div class="generation-box">
                     <a href="experienced-regalia">
                        <img src="img/generation-box3.png" alt="About Regalia">
                        <div class="gene-content">
                           <h5><strong>  About</strong></h5>
                           <h3>  REGALIA</h3>
                           <p>  The Habib regalia reflects the institution's core value of aesthetics, and the University's pedagogical focus on contextuality and inheritance.  </p>
                        </div>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Next Generation -->
      <div class="inner-pages-wraper pt-0">



         <!-- Division Box -->
         <!-- <section class="division-box graduate-box padtb-set pt-0">
            <div class="container">
               <div class="row justify-content-between fl-di-co-re">
                  <div class="col-lg-6 col-sm-6">
                     <div class="divsion-content">
                        <h3>Honorable Governor, Sindh</h3>
                        <h5>Imran Ismail</h5>
                        <p>Mr. Ismail began taking an active part in social and welfare work in 1996 as a volunteer for Shaukat Khanum Memorial Trust. In 2018 general elections, Mr. Ismail ran from PS-111 (previously PS-112) where he was elected as a member of Sindh Assembly, which he later resigned from to take up the responsibilities of the Governor of Sindh on August 27, 2018</p>
                        
                     </div>
                  </div>
                  <div class="col-lg-5 col-sm-6">
                     <div class="division-img">
                        <img src="img/convo21/1.png" alt="" class="division-mage">
                        <a data-fancybox="gallery"  href="https://www.youtube.com/watch?v=s0v2IE-ck0k&list=PLVyH-94EYRpqwQBH-JRPbXYIRpkdRpH_Z&index=19" class="play-icon-box">
                           <img src="img/playicon.svg" alt="">
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </section> -->
         <!-- Division Box -->
      
      
         <!-- Division Box -->   
         <!-- <section class="division-box graduate-box padb-set">
            <div class="container">
               <div class="row justify-content-between">
                  <div class="col-lg-5 col-sm-6">
                     <div class="division-img">
                        <img src="img/convo21/2.png" alt="" class="division-mage">
                        <a data-fancybox="gallery" href="https://www.youtube.com/watch?v=8c_Rx9XPVe4&list=PLVyH-94EYRpqwQBH-JRPbXYIRpkdRpH_Z&index=14" class="play-icon-box">
                           <img src="img/playicon.svg" alt="">
                        </a>
                     </div>
                  </div>
                  <div class="col-lg-6 col-sm-6">
                     <div class="divsion-content">
                        <h3>Keynote Speaker</h3>
                        <h5>Anwar Maqsood</h5>
                        <p>A renowned and highly respected scriptwriter, satirist, humorist and television presenter as the commencement speaker for the university’s second virtual and overall fourth convocation ceremony, celebrating the Pathfinders’ completion of their journey in the challenges and hardships of the global pandemic. Mr. Maqsood, popularly known for his fearlessness, caustic wit and love for telling the bitter tales addressed the Habib students in his signature style, using rich metaphors and poetic references making his speech a treat for the listeners.</p>
                       
                     </div>
                  </div>
               </div>
            </div>
         </section> -->
         <!-- Division Box -->
      
      
         <!-- Division Box -->   
         <!-- <section class="division-box graduate-box padb-set">
            <div class="container">
               <div class="row justify-content-between fl-di-co-re">
                  <div class="col-lg-6 col-sm-6">
                     <div class="divsion-content">
                        <h3>Valedictorian</h3>
                        <h5>Hasan Ul Haq</h5>
                        <h6> Habib University’s Valedictorian for 2021</h6>
                        <p>It is an honor to be standing here today, representing our collective journey as individuals who joined Habib in August 2017, and as the fourth batch of our university, we completed it and made our young university whole.” Habib University’s Valedictorian Hassan ul Haq from the Communication & Design Program (Class of 2021), reminisces about his special journey at the varsity and also applauded Pathfinders for accomplishing this memorable milestone.</p>
                     
                     </div>
                  </div>
                  <div class="col-lg-5 col-sm-6">
                     <div class="division-img">
                        <img src="img/convo21/3.png" alt="" class="division-mage">
                        <a data-fancybox="gallery" href="https://www.youtube.com/watch?v=u4hKu33bmwY&list=PLVyH-94EYRpqwQBH-JRPbXYIRpkdRpH_Z&index=16" class="play-icon-box">
                           <img src="img/playicon.svg" alt="">
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </section> -->
         <!-- Division Box -->
      
    
      
      
         <!-- Division Box -->   
         <!-- <section class="division-box graduate-box py-0">
            <div class="container">
               <div class="row justify-content-between">
                  <div class="col-lg-5 col-sm-6">
                     <div class="division-img">
                        <img src="img/convo21/5.png" alt="" class="division-mage">
                     </div>
                  </div>
                  <div class="col-lg-6 col-sm-6">
                     <div class="divsion-content">
                        <h3>Graduate Directory</h3>
                        <p>Find out more about our graduating class and their aspirations.</p>
                        <a href="" class="cooming-soon-btn">
                           <div class="btn-hover-down">
                              <span class="pdf-coming"><i class="far fa-file-pdf"></i>  Download Now</span>
                              <span class="pdf-download"><i class="fas fa-download"></i>  Download Now</span>
                           </div>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </section> -->
         <!-- Division Box -->
      </div>  

      <!-- Division Box -->
      <!-- <section class="division-box graduate-box">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo-homepage-img-2.png" alt="" class="division-mage">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Graduate Directory</h3>
                     <p>Find out more about our graduating class and <br> their aspirations.</p>
                     <a href="" class="cooming-soon-btn">
                        <div class="btn-hover-down">
                           <span class="pdf-coming"><i class="far fa-file-pdf"></i>  Coming Soon</span>
                           <span class="pdf-download"><i class="fas fa-download"></i>  Download Now</span>
                        </div>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <!-- Division Box -->
      <!-- Remember Dates Area -->
      <section class="remember-dates-area">
         <div class="container remember-dates-are-in">
            <h1>Graduation Ceremony<span> & Events</span></h1>
            <div class="row">
               <div class="col-lg-9 col-md-9">
                  <div class="graduation-ceremony">
                     <!-- <h2>Graduation Ceremony</h2> -->
                     <h4>Key Dates to Remember</h4>
                     <ul class="ceremony-list">
                        <li>
                           <span> Donor/Employer Event</span>
                           <span> 1<sup>st</sup> June 2022</span>
                           <!-- <span> Coming Soon</span> -->
                        </li>
                        <li>
                           <span>Awards Night</span>
                           <span>2<sup>nd</sup> June 2022</span>
                           <!-- <span> Coming Soon</span> -->
                        </li>
                        <li>
                           <span> Convocation Rehearsal</span>
                           <span> 3<sup>rd</sup> June 2022</span>
                           <!-- <span> Coming Soon</span> -->
                        </li>
                        <li>
                           <span> Convocation 2022</span>
                           <span> 4<sup>th</sup> June 2022</span>
                           <!-- <span> Coming Soon</span> -->
                        </li>
                        <!-- <li>
                           <span> Graduation ceremony</span>
                           <span> Coming Soon</span>
                           <span> Coming Soon</span>
                        </li> -->
                    </ul>
                  <a href="graduation-ceremony-events" class="cooming-soon-btn">
                     <div class="btn-hover-down">
                        <span class="pdf-coming"> See Details</span>
                        <span class="pdf-download">  See Details</span>
                     </div>
                  </a>
                  </div>
               </div>
               <div class="col-lg-5 col-md-5 cermony-img-parent">
                   <div class="cermony-cont">
                       <div class="cermony-img owl-carousel owl-theme">
                          <div class="cermony-slider-item">
                             <img class="img-fluid" src="img/cermony-right.png" alt="">
                             <div class="cermony-slider-item-content">
                                 <h3>Dummy Text</h3>
                                <p>Awarded annually to promising students & faculty.</p>
                             </div>
                          </div>
                          <div class="cermony-slider-item">
                             <img class="img-fluid" src="img/cermony-right.png" alt="">
                             <div class="cermony-slider-item-content">
                                <h3>Dummy Text</h3>
                                <p>Awarded annually to promising students & faculty.</p>
                             </div>
                          </div>
                       </div>
                   </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Remember Dates Area -->



   <!-- Insta feeds -->
   <section class="insta-feed-area" id="HUGRADS2022">
      <div class="sec-heading">
         <h1>#HUGRADS2022</h1>
      </div>
      <div class="container">
            <div class="insta-js-feeds">
            <?php 

               //query the user media
               $fields = "id,caption,media_type,media_url,permalink,thumbnail_url,timestamp,username";
               $token = "IGQVJVZAm5vWi0zZA0otUnVSR29mRHQ5U29fb2RWbGs3enZA0R2xMTjczQ0NOb2RDN1ZALWFVoTzhiT1VoYUEwQ09PcjNPWC1FWkRqN0ttNElMOW9ZAdDFoSTBWTExkaWJ1U0U0RXJsVU44emZAiZAnhlMmhnWQZDZD";
               $limit = 70;
               
               $json_feed_url="https://graph.instagram.com/me/media?fields={$fields}&access_token={$token}&limit={$limit}";
               $json_feed = @file_get_contents($json_feed_url);
               $contents = json_decode($json_feed, true, 512, JSON_BIGINT_AS_STRING);
               
               echo "<div class='ig_feed_container'>";
                  foreach($contents["data"] as $post){
                        
                     $username = isset($post["username"]) ? $post["username"] : "";
                     $caption = isset($post["caption"]) ? $post["caption"] : "";
                     $media_url = isset($post["media_url"]) ? $post["media_url"] : "";
                     $permalink = isset($post["permalink"]) ? $post["permalink"] : "";
                     $media_type = isset($post["media_type"]) ? $post["media_type"] : "";
                     $thumbnail_url = isset($post["thumbnail_url"]) ? $post["thumbnail_url"] : "";
                     $id = isset($post["id"]) ? $post["id"] : "";

                     $hashtag = "HabibUniversity";
                     $hashtag1 = "ShapingFutures";
                     if(strpos($caption, $hashtag) !== false || strpos($caption, $hashtag1) !== false){

                     echo "
                           <div class='ig_post_container'>
                          
                           <a class='insta-post-link' href='{$permalink}' target='_blank' data-toggle='modal' data-target='#exampleModal{$id}'>";
               
                                 if($media_type=="VIDEO"){
                                       echo "<video poster='{$thumbnail_url}' controls style='width:100%; display: block !important;'>
                                          <source src='{$media_url}' type='video/mp4'>
                                          Your browser does not support the video tag.
                                       </video>";
                                 }
               
                                 else{
                                       echo "<img src='{$media_url}' />";
                                 }
                              
                              echo "
                              
                              <div class='ig_post_details'>
                                    <div class='detail-iner'>
                                       <p>{$caption}</p>
                                    </div>
                                    <img class='insta-icon' src='img/insta-icon.svg' alt''/>
                              </div>
                              </a>
                           </div>

                           
                              <div class='modal fade' id='exampleModal{$id}' tabindex='-1' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                                    <div class='modal-dialog'>
                                       <div class='modal-content'>
                                             <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                                <span aria-hidden='true'>&times;</span>
                                             </button>
                                          <div class='modal-body'>
                                             <div class='popup-cotent'>
                                                   <div class='popup-img-cotent'>";
                                                  
                                                         
                                                   if($media_type=="VIDEO"){
                                                      echo "<video poster='{$thumbnail_url}' controls style='width:100%; display: block !important;'>
                                                         <source src='{$media_url}' type='video/mp4'>
                                                         Your browser does not support the video tag.
                                                      </video>";
                                                }
                              
                                                else{
                                                      echo "<img src='{$media_url}' />";
                                                }


                                                echo "   </div>
                                                   <div class='modal-right-content'>
                                                      <div class='modal-right--header'>
                                                           <img src='img/white-logo.svg' />
                                                           <h4>habibuniversity</h4>
                                                      </div>
                                                      <p>{$caption}</p>
                                                      <a href='{$permalink}' target='_blank' class='cooming-soon-btn'>
                                                      <div class='btn-hover-down'>
                                                         <span class='pdf-coming'>  View on Instagram</span>
                                                         <span class='pdf-download'>  View on Instagram</span>
                                                      </div>
                                                   </a>
                                                   </div>
                                             </div>
                                          </div>
                                          
                                       </div>
                                    </div>
                              </div>

                     ";

            }else{
               echo "";
           }
                   
                  }
               echo "</div>";

               echo "
               
               <div class='insta-view-more'>
                  <a id='loadMore' href='#' class='cooming-soon-btn'>
                     <div class='btn-hover-down'>
                        <span class='pdf-coming'>  View More</span>
                        <span class='pdf-download'>  View More</span>
                     </div>
                  </a>
               </div>
               
               ";
   
            ?>
            
            </div>     
         </div>
   
   </section>
   <!-- Insta feeds -->
 

   <?php include 'include/footer.php' ?>
  