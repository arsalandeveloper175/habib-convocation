
//   Home Page JS

$(function () {
   
$('.main-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<img src="img/left-arrow.svg">', '<img src="img/right-arrow.svg">'],
    dots: false,
    autoplay:true,
    autoplayHoverPause:false,
    mouseDrag: false,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },                   
        576:{
            items:1
        },
        768: {
            items: 1
        }, 
        992: {
            items: 1
        }
    }
});



AOS.init({
    disable: function() {
        return window.innerWidth < 800;
    }
});

if($(window).width() <= 768){
    if(('.coomon-responsove-slider').length != 0){
        $('.coomon-responsove-slider').addClass('owl-carousel owl-theme');
        $('.coomon-responsove-slider').owlCarousel({
            loop:false,
            margin:20,
            dots: true,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1
                }, 
                480:{

                    items:1
                },
                574:{

                    items:2
                },
                766:{
                    items:1
                }
            }
        });
    }
}

});





  $(document).ready(function(){
	$('#nav-icon3').click(function(){
		$(this).toggleClass('open');
	});

   
});