<footer>
      <div class="container">
         <div class="footer-inner">
             <p class="fol-para">Follow Habib University</p>
             <ul>
               <li><a target="_blank" href="https://www.facebook.com/HabibUniversity"> <img src="img/fb.png" alt=""> </a></li>
               <li><a target="_blank" href="https://www.linkedin.com/company/habib-university"><img src="img/linkedin.png" alt=""></a></li>
               <li><a target="_blank" href="https://instagram.com/habibuniversity"><img src="img/instagram.png" alt=""></a></li>
               <li><a target="_blank" href="https://twitter.com/habibuniversity"><img src="img/twitter.png" alt=""></a></li>
               <li><a target="_blank" href="https://www.youtube.com/user/HabibUni"><img src="img/youtube.png" alt=""></a></li>
            </ul>
             <p class="copy-right">© Habib University. All rights reserved.</p>
         </div>
      </div>
   </footer>
      <script src="js/plugin.js"></script>
      <script src="js/custom.js"></script>
      <!-- <script src="https://apps.elfsight.com/p/platform.js" defer></script> -->
   


      




   </body>
</html>