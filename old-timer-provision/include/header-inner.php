
      <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
      <!-- Template CSS -->
      <link rel="stylesheet" href="css/style.css?v=1.1.0">
      <link rel="stylesheet" href="css/breakpoints.css?v=1.1.0">
      <link rel="stylesheet" href="css/plugin.css?v=1.1.0">

      <!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-51636538-33"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-51636538-33');
</script>

   </head>
   <body>


   <!-- inner header -->
   <header class="full-menu">
      <div class="menu-bar-head">
         <div class="button_container" id="toggle">
            <div class="bar-lines">
               <span class="top"></span>
               <span class="middle"></span>
               <span class="bottom"></span>
            </div>
         </div>
      </div>
      
      <div class="overlay" id="overlay">
         <div class="covi-btn">

            <a target="_blank" href="https://habib.edu.pk/covid19/" class="cooming-soon-btn">
               <div class="btn-hover-down">
                  <span class="pdf-coming"><img src="img/covide-response.svg" alt="">   Covid-19 Response</span>
                  <span class="pdf-download"><img src="img/covide-response.svg" alt="">  Covid-19 Response</span>
               </div>
            </a>
         </div>
         <nav class="overlay-menu">
           <ul>
             <li >
                <a href="https://habib.edu.pk/convocation">
                  <span data-hover="Home">Home</span>    
               </a>
            </li>
             <li>
               <a href="experienced-regalia">
                   <span data-hover="Regalia">Regalia</span> 
               </a>
            </li>
             <li>
               <a href="anthem">
                  <span data-hover="Anthem">Anthem</span> 
              </a>
             </li>
             <li> 
               <a href="convocation-speaker">
                  <span data-hover="Convocation Speakers">Convocation Speakers</span> 
              </a>
             </li>
             <li>
               <a href="graduation-milestone">
                  <span data-hover="Graduation Milestone​s">Graduation Milestone​s</span> 
              </a>
             </li>
             
             <li>
               <a href="student-faculty-awards">
                  <span data-hover="Student & Faculty Awards">Student & Faculty Awards</span> 
              </a>
             </li>
             <li>
               <a href="graduation-ceremony-events">
                  <span data-hover="Ceremony and Events">Ceremony and Events</span> 
              </a>
             </li>
             <li>
               <a href="index#HUGRADS2022">
                  <span data-hover="
                  #HUGRADS2022"> #HUGRADS2022</span> 
              </a>
             </li>
            
             <!-- <li>
               <a href="graduate-directory">
                  <span data-hover="Graduate Directory">Graduate Directory</span> 
              </a>
             </li> -->
           </ul>
         </nav>
       </div>
   </header>
   <!-- inner header -->