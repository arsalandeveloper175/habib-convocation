
<?php include 'include/header-inner.php' ?>


   <!-- Inner Banner -->
   <section class="inner-banner awards-banner">
      <div class="conatiner">
         <div class="iner-baner--content">
            <h1>
               Convo21
               <span>Announcement</span>
            </h1>
         </div>
      </div>
   </section>

   <!-- Inner Banner -->

<!-- Anouncement Area -->
  <section class="announcement-area">
     <div class="container mb-4">
        <div class="row">
           <div class="col-lg-5">
              <div class="search-anouncement">
               <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="Search">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                  </div>
                </div>
               </div>
           </div>
        </div>
     </div>
     <div class="container">
         <div class="anoun-list-item">
            <div class="anoun-cont-iner">
               <img src="img/announcement/anoun-thumb1.png" class="img-fluid announ-thumnail" alt="">
               <div class="anoun-item-content">
                  <h3>Announcement Title</h3>
                  <p>02/03/2021</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
               </div>
            </div>
            <div class="detail-btn">
               <a href="" class="cooming-soon-btn">
                  <div class="btn-hover-down">
                     <span class="pdf-coming"><i class="far fa-file-pdf"></i>  Download Now</span>
                     <span class="pdf-download"><i class="fas fa-download"></i>  Download Now</span>
                  </div>
               </a>
               <a href="" class="cooming-soon-btn">
                  <div class="btn-hover-down">
                     <span class="pdf-coming"> Watch Video</span>
                     <span class="pdf-download">Watch Video</span>
                  </div>
               </a>
               <a href="" class="cooming-soon-btn">
                  <div class="btn-hover-down">
                     <span class="pdf-coming"> Read More</span>
                     <span class="pdf-download"> Read More</span>
                  </div>
               </a>
            </div>
         </div>

         
         <div class="anoun-list-item">
            <div class="anoun-cont-iner">
               <img src="img/announcement/default-thumb.png" class="img-fluid announ-thumnail" alt="">
               <div class="anoun-item-content">
                  <h3>Announcement Title</h3>
                  <p>02/03/2021</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
               </div>
            </div>
            <div class="detail-btn">
               <a href="" class="cooming-soon-btn">
                  <div class="btn-hover-down">
                     <span class="pdf-coming"> Watch Video</span>
                     <span class="pdf-download">Watch Video</span>
                  </div>
               </a>
               <a href="" class="cooming-soon-btn">
                  <div class="btn-hover-down">
                     <span class="pdf-coming"> Read More</span>
                     <span class="pdf-download"> Read More</span>
                  </div>
               </a>
            </div>
         </div>

         
         <div class="anoun-list-item">
            <div class="anoun-cont-iner">
               <img src="img/announcement/anoun-thumb2.png" class="img-fluid announ-thumnail" alt="">
               <div class="anoun-item-content">
                  <h3>Announcement Title</h3>
                  <p>02/03/2021</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
               </div>
            </div>
            <div class="detail-btn">
               <a href="" class="cooming-soon-btn">
                  <div class="btn-hover-down">
                     <span class="pdf-coming"> Read More</span>
                     <span class="pdf-download"> Read More</span>
                  </div>
               </a>
            </div>
         </div>


         
         <div class="anoun-list-item">
            <div class="anoun-cont-iner">
               <img src="img/announcement/anoun-thumb3.png" class="img-fluid announ-thumnail" alt="">
               <div class="anoun-item-content">
                  <h3>Announcement Title</h3>
                  <p>02/03/2021</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
               </div>
            </div>
            <div class="detail-btn">
               <a href="" class="cooming-soon-btn">
                  <div class="btn-hover-down">
                     <span class="pdf-coming"><i class="far fa-file-pdf"></i>  Download Now</span>
                     <span class="pdf-download"><i class="fas fa-download"></i>  Download Now</span>
                  </div>
               </a>
               <a href="" class="cooming-soon-btn">
                  <div class="btn-hover-down">
                     <span class="pdf-coming"> Read More</span>
                     <span class="pdf-download"> Read More</span>
                  </div>
               </a>
            </div>
         </div>
        
     </div>
  </section>
<!-- Anouncement Area -->





<?php include 'include/footer.php' ?>