
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University &#8211; Convocation 2021 &#8211; Habib University</title>
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
<?php include 'include/header-inner.php' ?>




     <!-- Inner Banner -->
     <section class="graduation-miles-banner conv-spea-banner">
      <div class="container">
         <div class="row align-content-center">
            <div class="col-lg-6">
               <div class="banner-cont-grad">
                  <h1 class="banner-title">
                     Graduation  
                   <span>Ceremony & Events</span>
                  </h1>
               </div>
            </div>
            <div class="col-lg-6">
               <img src="img/events/banner.png" class="img-fluid" alt="">
            </div>
         </div>
      </div>
   </section>
 
    <!-- Inner Banner -->

   <section class="para-area">
      <div class="container">
         <section class="sec-heading">
            <h2 class="shedule-heading">
               Date/schedule will be available here at the time of Convocation.
            </h2>
         </section>
      </div>
    </section>

   <section class="graduation-events" style="display: none;">
      <div class="container">
         <div class="row">
            <div class="col-lg-8">
               <div class="event-gradu-boxes">
                  <h2>Graduation Events</h2>
                  <div class="event-box">
                     <span>Student Yearbook</span>
                     <span>5th June</span>
                     <span>7:00 PM - 8:30 PM </span>
                  </div>
                  <div class="event-box">
                     <span>Faculty Gratitude (online)</span>
                     <span>7th June</span>
                     <span>7:00 PM - 8:30 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Faculty & Seniors Karaoke</span>
                     <span>9th June</span>
                     <span>7:00 PM - 8:30 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Virtual Concert</span>
                     <span>9th June</span>
                     <span>Watch Online</span>
                  </div>
                  <div class="event-box">
                     <span>Alvida- Virtual Farewell (online)</span>
                     <span>11th June</span>
                     <span>7:00 PM - 8:30 PM</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-10">
               <div class="event-gradu-boxes">
                  <h2>Graduation Ceremony - 12th June , 2021</h2>
                  <div class="event-box">
                     <span>Welcome & Tilawat</span>
                     <span>5:00 PM</span>
                  </div>
                  <div class="event-box">
                     <span>National Anthem</span>
                     <span>5:05 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Declaration of Convocation Open</span>
                     <span>5:07 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Tribute to the Pathfinders - Video</span>
                     <span>5:08 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Valedictorian Announcement & Speech</span>
                     <span>5:10 PM </span>
                  </div>
                  <div class="event-box">
                     <span>Convocation Address by President, Mr. Wasif A. Rizvi</span>
                     <span>5:16 PM </span>
                  </div>
                  <div class="event-box">
                     <span>Tribute to Faculty – Video</span>
                     <span>5:28 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Celebrating Excellence in Teaching</span>
                     <span>5:30 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Commencement Speech</span>
                     <span>5:33 PM</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-10">
               <div class="event-gradu-boxes">
                  <h2>Conferring of Degrees</h2>
                  <div class="event-box">
                     <span>Dhanai School of Science and Engineering</span>
                     <span>5:43 PM</span>
                  </div>
                  <div class="event-box">
                     <span>School Awards (DSSE)</span>
                     <span>5:49 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Dean's Awards (DSSE)</span>
                     <span>5:51 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Speech by Governor Sindh, Mr. Imran Ismail</span>
                     <span>5:52 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Student Government Awards</span>
                     <span>5:59 PM</span>
                  </div>
                  <div class="event-box">
                     <span>School of Arts, Humanities and Social Science</span>
                     <span>6:01 PM</span>
                  </div>
                  <div class="event-box">
                     <span>School Awards (AHSS)</span>
                     <span>6:07 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Dean's Awards (AHSS)</span>
                     <span>6:09 PM</span>
                  </div>
                  <div class="event-box">
                     <span>President’s Award</span>
                     <span>6:10 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Speech by Chancellor, Mr. Rafiq M. Habib</span>
                     <span>6:12 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Chancellor's Yohsin Award</span>
                     <span>6:19 PM</span>
                  </div>
                  <div class="event-box">
                     <span>HU Anthem – Video</span>
                     <span>6:21 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Declaration of Convocation Closed</span>
                     <span>6:26 PM</span>
                  </div>
                  <div class="event-box">
                     <span>Students Celebration/Cap-throwing</span>
                     <span>6:28 PM</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>


  

<?php include 'include/footer.php' ?>