
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation – 2019</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->


<?php include 'include/header-inner.php' ?>



   <!-- inner banner -->
   <!-- <section class="inner-banner banner-19">
      <div class="conatiner">
         <div class="iner-baner--content">
            <h1>
               Convocation
               <span>2019</span>
            </h1>
         </div>
      </div>
   </section> -->
   <!-- inner banner -->


   <section class="para-area">
     <div class="container">
        <section class="sec-heading">
           <!-- <h5>Be the next generaton of</h5> -->
           <h1>#HUGRADS2019</h1>
           <p>On June 15, 2019, Habib University conducted its second convocation to celebrate the Class of <br> 2019  – the Pioneers – and recognize their achievements</p>
        </section>
     </div>
   </section>


   <div class="inner-pages-wraper">

      
      <!-- Inner Video -->
      <section class="main-iner-video">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="video-banner">
                     <img src="img/convo19/video-banner-2019.jpg" class="img-fluid iner-banner" alt="">
                     <img src="img/convo19/video-banner-2019-resp.jpg" class="img-fluid inner-res-banner" alt="">
                     <a class="play-icon-box" href="https://youtu.be/nHTD-0kcBcs" data-fancybox="gallery">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Inner Video -->
   
      <!-- Division Box -->   
      <section class="division-box graduate-box padt-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo19/2.png" alt="Dr. Reza Aslan" class="division-mage">
                     <a  href="https://youtu.be/UjeEb51OW4Q" data-fancybox="gallery" class="play-icon-box">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Keynote Speaker</h3>
                     <h5>Dr. Reza Aslan</h5>
                     <h6>Internationally renowned Iranian-American writer, professor and scholar of religious history</h6>
                     <p>Internationally renowned Iranian-American writer, professor and scholar of religious history. He is the author of a number of international bestsellers, including No god but God: The Origins, Evolution, and Future of Islam, Zealot: The Life and Times of Jesus of Nazareth and more recently God: A Human History.</p>
                    
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->

      <!-- Division Box -->
      <section class="division-box graduate-box padtb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo19/1.png" alt="Imran Ismail" class="division-mage">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Honorable Governor, Sindh</h3>
                     <h5>Imran Ismail</h5>
                     <p class="mb-4">Born on January 1, 1966, in Karachi, Imran Ismail is the 33rd Governor of Sindh. He completed his graduation from Government National College, Karachi, and later moved abroad to Italy and the United States, where he studied chemical and leather technology.</p>
                     <p>Mr. Ismail began taking an active part in social and welfare work in 1996 as a volunteer for Shaukat Khanum Memorial Trust. In 2018 general elections, Mr. Ismail ran from PS-111 (previously PS-112) where he was elected as a member of Sindh Assembly, which he later resigned from to take up the responsibilities of the Governor of Sindh on August 27, 2018.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->
   

   
   
      <!-- Division Box -->   
      <section class="division-box graduate-box padb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo19/3.png" alt="Syed Maisam Hyder Ali" class="division-mage">
                     <a  href="https://youtu.be/HJKPMjyphm0" data-fancybox="gallery" class="play-icon-box">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Valedictorian</h3>
                     <h5>Syed Maisam Hyder Ali</h5>
                     <h6>Valedictorian, Class of 2019</h6>
                     <p>“Habib University has always motivated and inspired me to deliver my best. My time here has been spent discovering ideas, uncovering philosophies, and studying literatures to prepare me for a future I desired. It taught me the significance of our traditions and values and that is what makes Habib University a unique institution.”</p>
                    
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->
   
      <!-- Division Box -->   
      <section class="division-box graduate-box padb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/convo19/4.png" alt="Awards Night" class="division-mage">
                     <a  href="https://youtu.be/1mo0jN9Ixws" data-fancybox="gallery" class="play-icon-box">
                        <img src="img/playicon.svg" alt="">
                     </a>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Awards Night</h3>
                     <p>In recognition of the high academic achievements of the Class of 2019, Habib University awards night ceremony on its campus. The event was one in which this year’s graduates, the Pioneers, were able to reflect on the past four years and how their academic attainments and hard work have helped not only their own personal success, but also that of the University.</p>
                    
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->
   


      <section class="graduate-area">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Graduate Directory</h3>
                     <p>The graduate directory serve as depository of information for recruitment by potential employers.</p>
                     <a target="_blank" href="https://habib.edu.pk/career-services/request-for-graduate-directory/" target="_blank" class="cooming-soon-btn">
                        <div class="btn-hover-down">
                           <span class="pdf-coming"><i class="far fa-file-pdf"></i>  Download Now</span>
                           <span class="pdf-download"><i class="fas fa-download"></i>  Download Now</span>
                        </div>
                     </a>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="grdaute-heading">
                   <img src="img/graduate-text.svg" alt="">
                  </div>
               </div>
            </div>
         </div>
      </section>



   </div>  



<?php include 'include/footer.php' ?>