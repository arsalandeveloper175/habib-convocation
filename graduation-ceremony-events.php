
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation - Graduation Events</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
<?php include 'include/header-inner.php' ?>


  <section class="graduation-miles-banner banner-29">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-8">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                    Graduation  
                    <span>Ceremony & Events</span>
                 </h1>
              </div>
           </div>
           <!-- <div class="col-lg-6">
              <img src="img/graudation-miles/banner.png" class="img-fluid" alt="">
           </div> -->
        </div>
     </div>
  </section>



  <section class="graduation-para event-area ">
     <div class="container">
        <div class="row padtb-set">
           <div class="col-lg-12">
              <div class="inner-grad-cont">
                 <h4>Graduation Events</h4>
               <ul class="gradua-event-list">
                  <li> 
                     <span class="text-ceremony"><b>Activity Detail</b></span> 
                     <span class="time-ceremony date-time"><b>Date</b></span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Stand-Up</span> 
                     <span class="time-ceremony"> Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Concert</span> 
                     <span class="time-ceremony"> Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Scavenger Hunt/Hit-Posting Show</span> 
                     <span class="time-ceremony"> Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Donor/Employer Event</span> 
                     <span class="time-ceremony"> 1st June 2022</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Awards Night</span> 
                     <span class="time-ceremony"> 2nd June 2022</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Convocation Rehearsal</span> 
                     <span class="time-ceremony"> 3rd June 2022</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Convocation 2022</span> 
                     <span class="time-ceremony"> 4th June 2022</span> 
                  </li>
               </ul>
              </div>
           </div>
        </div>

     </div>
  </section>
  <!-- <section class="graduation-para event-area">
     <div class="container">
        <div class="row padb-set">
           <div class="col-lg-12">
              <div class="inner-grad-cont">
                 <h4>Graduation Ceremony </h4>
                 <p>Minute to Minute Program</p>
               <ul class="gradua-event-list">
                  <li> 
                     <span class="text-ceremony"><b>Activity Detail</b></span> 
                     <span class="time-ceremony date-time"><b>Time</b></span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Reporting time for graduands (Class of 2022)</span> 
                     <span class="time-ceremony">1530 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Distribution of robes and regalia for students</span> 
                     <span class="time-ceremony">1530 hrs - 1730 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Group photo session for Class of 2022 and faculty (at Amphitheater)</span> 
                     <span class="time-ceremony">1730 hrs - 1800 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Arrival of guests not accompanying graduates to be seated in the Convocation arena</span> 
                     <span class="time-ceremony">1800 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Formation of the procession; (in the following order) Registrar, Graduating Students, Lecturers, Sr. Lecturers, Assistant Professors, Associate Professors, Assistant Deans, Associate Dean, VPAA/DoF, President, Chancellor, BoGs</span> 
                     <span class="time-ceremony">1810 hrs - 1830 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Announcement for guests to be seated in the Convocation arena.</span> 
                     <span class="time-ceremony">1830 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Introduction by Master of Ceremonies (MoCs)</span> 
                     <span class="time-ceremony">1832 hrs - 1835 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs announces Academic Procession to enter the event arena </span> 
                     <span class="time-ceremony">1835 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">FGraduands to enter and to be seated in designated areas</span> 
                     <span class="time-ceremony">1835 hrs - 1845 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs request for selected student to come on stage for Quranic recitation</span> 
                     <span class="time-ceremony">1845 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Quranic recitation with translation in English and Urdu</span> 
                     <span class="time-ceremony">1845 hrs - 1850 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs requests participants to rise for the National Anthem</span> 
                     <span class="time-ceremony">1851 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Playing of recording of Pakistan National Anthem</span> 
                     <span class="time-ceremony">1851 hrs - 1852 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs request University Chancellor to formally declare the Convocation open</span> 
                     <span class="time-ceremony">1853 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">University Chancellor to declares the Convocation open</span> 
                     <span class="time-ceremony">1853 hrs - 1854 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs invite Valedictorian (Class of 2022) to come on stage for the Valedictorian Address</span> 
                     <span class="time-ceremony">1854 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Valedictorian Address</span> 
                     <span class="time-ceremony">1855 hrs - 1900 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs introduce and invite the University President on stage for the President’s Address</span> 
                     <span class="time-ceremony">1901 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">President’s Address</span> 
                     <span class="time-ceremony">1901 hrs - 1915 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs invite VPAA/DOF to introduce the Guest of Honor and Commencement Speaker</span> 
                     <span class="time-ceremony">1916 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">VPAA/DOF introduces and invites the Guest of Honor to on stage for the Commencement Address</span> 
                     <span class="time-ceremony">1916 hrs - 1920 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Commencement Address</span> 
                     <span class="time-ceremony">1920 hrs - 1935 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs to commence the degree awarding ceremony</span> 
                     <span class="time-ceremony">1936 hrs.</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoC announces names of DSSE Graduates (2022) for awarding of degrees</span> 
                     <span class="time-ceremony">1936 hrs - 1951 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs introduce and invite the Hon. Governor of Sindh</span> 
                     <span class="time-ceremony">1952 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Governor’s Address</span> 
                     <span class="time-ceremony">1953 hrs - 2000 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs announce names of SAHSS Graduates (2022) for awarding of degrees</span> 
                     <span class="time-ceremony">2001 hrs - 2015 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs invite Assoc. Dean (T&L) on stage to announce the Faculty Awards (2022)</span> 
                     <span class="time-ceremony">2016 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Announcement of Faculty Awards</span> 
                     <span class="time-ceremony">2016 hrs - 2021 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs invite VPAA/DOF on stage to announce the Chancellor’s Yohsin Medal Recipient (Class of 2022)</span> 
                     <span class="time-ceremony">2022 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Announcement of Chancellor’s Yohsin Medal Recipient (Class of 2022)</span> 
                     <span class="time-ceremony">2022 hrs - 2023 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs invite the University Chancellor for the Chancellor’s Address</span> 
                     <span class="time-ceremony">2023 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Chancellor’s Address</span> 
                     <span class="time-ceremony">2023 hrs - 2028 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Playing of HU Anthem</span> 
                     <span class="time-ceremony">2028 hrs - 2030 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs request University Chancellor to formally declare the Convocation closed</span> 
                     <span class="time-ceremony">2031 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoCs announce departure of Academic Procession (in reverse order: Registrar, Chancellor, President, VPAA/DoF, Assoc. Deans, Assistant Deans, Associate Professors, Assistant Professor, Sr. Lecturers, Lecturers, Students)</span> 
                     <span class="time-ceremony">2033 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">MoC announces end of Convocation Ceremony, dining arrangements and exits and any other relevant announcements.</span> 
                     <span class="time-ceremony">2033 hrs</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Graduands to move to the lawn area for photography of cap-throwing etc.</span> 
                     <span class="time-ceremony">2033 hrs - 2100 hrs</span> 
                  </li>
               </ul>
              </div>
           </div>
        </div>

     </div>
  </section> -->
   
  <!-- <section class="graduation-para event-area">
     <div class="container">
        <div class="row padb-set">
           <div class="col-lg-12">
              <div class="inner-grad-cont">
                 <h4>Conferring of Degrees </h4>
               <ul class="gradua-event-list">
                  <li> 
                     <span class="text-ceremony">Dhanani School of Science and Engineering</span> 
                     <span class="time-ceremony">Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">School of Arts, Humanities and Social Sciences</span> 
                     <span class="time-ceremony">Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Speech by Governor Sindh, Mr. Imran Ismail</span> 
                     <span class="time-ceremony">Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Dean’s Awards</span> 
                     <span class="time-ceremony">Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">School Awards</span> 
                     <span class="time-ceremony">Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Student Life Awards</span> 
                     <span class="time-ceremony">Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Commencement Speech, Dr. Azra Raza</span> 
                     <span class="time-ceremony">Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Speech by Chancellor, Mr. Rafiq Muhammad Habib</span> 
                     <span class="time-ceremony">Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Chancellor’s Yohsin Award</span> 
                     <span class="time-ceremony">Coming Soon</span> 
                  </li>
                  <li> 
                     <span class="text-ceremony">Declaration of Convocation Closed</span> 
                     <span class="time-ceremony">Coming Soon</span> 
                  </li>
               </ul>
              </div>
           </div>
        </div>

     </div>
  </section> -->

<?php include 'include/footer.php' ?>