
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="Donate, Pakistan, Education, higher education, HUFUS,HUF US,Habib University Foundation,Habib University Foundation US, Habib,Habib Donors,Contribute to Habib,Habib University Houston,Habib University Fundraiser,Habib University,Houston, ">
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->
      <title>Habib University – Convocation - Speakers</title>
      <meta name="keywords" content="Habib University, Convocation, Commencement, Degree Distribution, Karachi, Liberal Arts University" />
      <meta name="description" content="Habib University graduates discover a new path or way, these graduates have the skills, knowledge and courage to discover new paths, a testimony to the cutting-edge liberal arts and sciences education provided by Habib University.">
      <meta name="author" content="">
      <meta property="og:image" content="https://habib.edu.pk/convocation/img/logo.png" />
      <!-- ========== TITLE, DESCRIPTION & OTHER META TAGS ========== -->

<?php include 'include/header-inner.php' ?>


   <!-- Inner Banner -->
  <!-- <section class="graduation-miles-banner conv-spea-banner">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-6">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                  Convocation  
                  <span>Speakers</span>
                 </h1>
              </div>
           </div>
           <div class="col-lg-6">
              <img src="img/speakers/banner.jpg" class="img-fluid" alt="">
           </div>
        </div>
     </div>
  </section> -->

  
  <section class="graduation-miles-banner banner-27">
     <div class="container">
        <div class="row align-content-center">
           <div class="col-lg-6">
              <div class="banner-cont-grad">
                 <h1 class="banner-title">
                 Convocation   
                    <span>Speakers</span>
                 </h1>
              </div>
           </div>
        </div>
     </div>
  </section>

   <!-- Inner Banner -->


   <div class="inner-pages-wraper pt-0">
      <!-- Division Box -->
      <!-- <section class="division-box graduate-box padt-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/speakers/1.png" alt="" class="division-mage">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Anwar Maqsood - Commencement Speaker</h3>
                     <h5>Scriptwriter, Television Presenter, Satirist, Humorist, and Infrequent Actor</h5>
                     <p class="mb-4">Anwar Maqsood is one of the most --revered literary personalities in the country. A renowned and highly respected scriptwriter, satirist, humorist and a television presenter he has been the conscience of our nation for well over 50 years. His deep understanding of the ailments of our society accompanied by his dry wit have made us, even if unadmittedly look inwards and ask ourselves about our contribution to our society.</p>
                     <p>The Government of Pakistan has recognized his contribution to society and the field of entertainment in particular by awarding him the Pride of Performance in 1994 and the Hilal-e-Imtiaz I 2013.</p>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <!-- Division Box -->
   
     <!-- Division Box -->
     <section class="division-box graduate-box padt-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/speakers/5.png" alt="" class="division-mage">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Kamal Ahmad </h3>
                     <h5>Founder, Asian University for Women (AUW) <br>
                     Chittagong Bangladesh </h5>
                     <p class="mb-4">Kamal Ahmad is the Founder of the Asian University for Women (AUW) and leads its strategic planning and fundraising operations as President & CEO of the Asian University for Women Support Foundation. </p>
                     <p>Mr Ahmad holds a Bachelor’s degree from Harvard University and a Juris Doctor from the Michigan Law School and has served on the staffs of the World Bank, Rockefeller Foundation, UNICEF, and the General Counsel of the Asian Development Bank. He is a recipient of a number of awards including the United Nations Gold Peace Medal & Citation Scroll, given by the Paul G. Hoffman Awards Fund; Time magazine College Achievement Award; World Economic Forum Global Leader for Tomorrow Award; and the John Phillips Award from the Phillips Exeter Academy. </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->

      
       <!-- Division Box -->
       <!-- <section class="division-box graduate-box padt-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/speakers/2.png" alt="" class="division-mage">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Imran Ismail</h3>
                     <h5>Honorable Governor, Sindh</h5>
                     <p class="mb-4">Born on January 1, 1966, in Karachi, Imran Ismail is the 33rd Governor of Sindh. He completed his graduation from Government National College, Karachi, and later moved abroad to Italy and the United States, where he studied chemical and leather technology.</p>
                     <p>Mr. Ismail began taking an active part in social and welfare work in 1996 as a volunteer for Shaukat Khanum Memorial Trust. In 2018 general elections, Mr. Ismail ran from PS-111 (previously PS-112) where he was elected as a member of Sindh Assembly, which he later resigned from to take up the responsibilities of the Governor of Sindh on August 27, 2018.</p>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <!-- Division Box -->

       <!-- Division Box -->
       <section class="division-box graduate-box padtb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/speakers/4.png" alt="" class="division-mage" alt="Wasif A. Rizvi">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Wasif A. Rizvi</h3>
                     <h5>President</h5>
                     <p class="mb-4">Wasif Rizvi is the founding president of Habib University, Pakistan’s first undergraduate liberal arts and science institution. He is an ardent advocate of providing students a student-centric, interdisciplinary and contextual intellectual experience.</p>
                     <p class="mb-4">His achievements are visible through Habib University’s unmatched global partnerships, distinguished faculty, innovative curriculum, diverse student body and an award winning campus design.</p>
                     <p>He holds twin graduate degrees from Harvard Kennedy School and Harvard School of Education and prior to founding Habib University was associated with the Aga Khan Education Service in Pakistan and many other development projects across Asia and Africa over the last two decades.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->


      <!-- Division Box -->
      <section class="division-box graduate-box padb-set">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-lg-5 col-sm-6">
                  <div class="division-img">
                     <img src="img/speakers/3.png" alt="" class="division-mage" alt="Rafiq M. Habib">
                  </div>
               </div>
               <div class="col-lg-6 col-sm-6">
                  <div class="divsion-content">
                     <h3>Rafiq M. Habib</h3>
                     <h5>Chancellor</h5>
                     <p>Mr. Rafiq M. Habib is Chancellor of Habib University and Chairman of the Habib University Foundation. As the former head of the House of Habib, he possesses decades of business experience in insurance, banking and industry. Mr. Habib studied at St. Patrick High School, where after completing a Diploma Course in Banking, he later qualified for the Associate Membership of the Life Insurance Management Institute, New York. Subsequently, he also completed the first Advance Management Program conducted by the Harvard Business School, USA in Pakistan.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Division Box -->
   



   </div>

   

<?php include 'include/footer.php' ?>